import { useContext, Fragment } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from "./UserContext";

export default function AppNavbar (){

	// const [ email, setEmail ] = useState(localStorage.getItem('email'));

	const { user } = useContext(UserContext);
	console.log(user);

	// function refreshPage() {
	// 	window.location.reload(false);
	//  }

	return(
			<Navbar bg="light" expand="lg">
				<Navbar.Brand as={ Link } to ='/'>ZUITT</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={ NavLink } to ='/' exact>Home</Nav.Link>
						<Nav.Link as={ NavLink } to ='/courses' exact>Courses</Nav.Link>

						{/* { (user.email !== null) ? */}
						{ (user.id !== null) ?
						<Nav.Link as={NavLink} to='/logout' exact>Logout</Nav.Link>
						//<Nav.Link as={NavLink} to='/logout' exact onClick={refreshPage}>Logout</Nav.Link>
						:
						<Fragment>
							<Nav.Link as={NavLink} to='/login' exact>Login</Nav.Link>
							<Nav.Link as={NavLink} to='/register' exact>Register</Nav.Link>
						</Fragment>
						}

						{/* modified: href to { Link } */}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}

