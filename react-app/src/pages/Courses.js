import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
// import coursesData from '../data/coursesData';

export default function Courses() {
   // console.log(coursesData);
   // return (
   //    <h1>Courses</h1>
   // )

   const [ courses, setCourses ] = useState([]);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=> {
			console.log(data);

			setCourses(data.map(course=>{
				return(
					<CourseCard key={course._id} courseProp={course} />	
				);
			}));
		})
	},[])

   // **comment out after, useEffect (fetch)
   // const courses = coursesData.map(course => {
   //    return (
   //       <CourseCard key = {course.id} courseProp={course}/>
   //    )
   // })

   // Mini §§activity - moved CourseCard from Home (pages) to Courses
   return (
      <Fragment>
         {courses}
      </Fragment>
   )
};


