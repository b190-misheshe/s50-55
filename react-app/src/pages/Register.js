import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext'
import { Navigate } from 'react-router-dom'

export default function Register() {

  const { user, setUser } = useContext(UserContext);
  console.log(setUser);

   const [email, setEmail] = useState('');
   const [password1, setPassword1] = useState('');
   const [password2, setPassword2] = useState('');
   const [isActive, setIsActive] = useState(false);

   console.log(email);
   console.log(password1);
   console.log(password2);

   useEffect(() => {
      if (email !== "" && password1 !== "" && password2 !== "" && password1 === password2) {
      // submit button
         setIsActive(true);
      } else {
         setIsActive(false);
      }
   }, [email, password1, password2]);  //dependency array

   function registerUser(e) {
      e.preventDefault();

      // clear input details
      setEmail("");
      setPassword1("");
      setPassword1("");

      alert("Thank you for registering!");
   }

   return (
      (user.email !== null)?
      <Navigate to='/courses' />
      :
      <Form onSubmit={(e) => registerUser(e)}>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
               type="email"
               placeholder="Enter email"
               value={email}
               onChange={e => setEmail(e.target.value)}
               required/>
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
  
        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control  
               type="password"
               placeholder="Password"
               value={password1}
               onChange={e => setPassword1(e.target.value)}
               required/>
        </Form.Group>

        <Form.Group controlId="password2">
          <Form.Label> Verify Password</Form.Label>
          <Form.Control
               type="password"
               placeholder="Password"
               value={password2}
               onChange={e => setPassword2(e.target.value)}
               required/>
        </Form.Group>

         {isActive ?
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        :
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
         }
      </Form>
    );
};
