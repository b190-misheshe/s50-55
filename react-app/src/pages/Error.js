import React, { Fragment } from 'react'
// import { Row, Col, Nav } from 'react-bootstrap';
// import { NavLink } from 'react-router-dom';

// import Banner from "../components/Banner";


const Error = () => {

  const data = {
    title: '404 Not Found',
    content: 'The page you are looking for cannot be found',  
    destination: '',
    label: ''
  }

  return (
    <Fragment>
      { data }
    </Fragment>



  //   <Row>
  //   <Col className="p-5">
  //      <h1>Page Not Found.</h1>
  //      <p>Go back to the
  //      <a href="/"> homepage.
  //      </a>     
  //      </p>   
  //      {/* <a>
  //      <Nav.Link as={ NavLink } to ='/'>homepage</Nav.Link>
  //      </a> */}
  //      {/* <p>Go back to the  <Nav.Link as={ NavLink } to ='/' exact>homepage</Nav.Link></p>    */}
  //   </Col>
  // </Row>

  )
}

export default Error