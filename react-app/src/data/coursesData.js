const coursesData = [
   {
      id: "wdc001",
      name: "PHP-Laravel",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sodales sodales libero imperdiet ultrices. Duis tempus at nisi suscipit maximus. Nunc ultrices nunc sed augue rhoncus bibendum. Etiam posuere nisl eu nisi tempor placerat. Sed vehicula feugiat risus a tempor.",
      price: 45000,
      onOffer: true
   },
   {
      id: "wdc002",
      name: "Python-Django",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sodales sodales libero imperdiet ultrices. Duis tempus at nisi suscipit maximus. Nunc ultrices nunc sed augue rhoncus bibendum. Etiam posuere nisl eu nisi tempor placerat. Sed vehicula feugiat risus a tempor.",
      price: 45000,
      onOffer: true
   },
   {
      id: "wdc003",
      name: "Java-Springbot",
      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sodales sodales libero imperdiet ultrices. Duis tempus at nisi suscipit maximus. Nunc ultrices nunc sed augue rhoncus bibendum. Etiam posuere nisl eu nisi tempor placerat. Sed vehicula feugiat risus a tempor.",
      price: 45000,
      onOffer: true
   }
]

export default coursesData;
