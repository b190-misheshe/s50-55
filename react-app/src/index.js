import React from 'react';
import ReactDOM from 'react-dom/client';

// Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'

import App from './App';
// import AppNavBar from "./AppNavBar";
// import Banner from "./components/Banner";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
);



