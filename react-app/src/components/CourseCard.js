import { Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

// we can try to destructure the props object recieved from the parent component(Courses.js) so that the main object from the mock database will be accessed
export default function CourseCard({courseProp}) {

    console.log(courseProp);    
    console.log(typeof courseProp);

    // destructure the courseProp object for more code readability and ease of coding for the part of the dev (doesn't have to access multiple objects before accessing the needed properties)
    const { name, description, price, _id } = courseProp;
// /*/*
// /*
//     use the state hook for this component to be able to store its state
//     States are used to keep track of the information related to individual components
//         SYNTAX: const [ getter, setter ] =useState(initialGetterValue)
// */
//     const [ count, setCount ] = useState(0);
//     // this returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
//     console.log(useState(0))

//     const [ seats, setSeats ] = useState(10);
//     // create another useState to refactor the enroll function
//     const [ isOpen, setIsOpen ] = useState(true);


    
//         this function keeps track of the enrollees for a course
//         the setter function for useStates are asynchronous allowing it to be executed separately from other codes ("setCount" is executed while the "console.log" is already completed) 
    
//     function enroll(){
//         setCount(count + 1);
//         console.log(`Enrollees: ${count}`);
//         setSeats(seats - 1);
//         console.log(`Seats: ${seats}`);
//     }
//     /*
//         effect hook - allows us to execute a piece of code whenever a component gets rendered or if a value of the state changes
//         useEffect - requires two arguments: function and a dependency array: BOTH ARE NEEDED
//             function - to set which lines of codes are to be executed
//             dependency array - identifies which variable/s are to be listened to, in terms of changing the state, for the function to be executed.
//                 if the array is left empty, the codes will not listen to any variable but the codes will be executed once.
//     */
//     useEffect(()=>{
//         if(seats === 0){
//             setIsOpen(false);
//         }
//     }, [seats]);*/*/
/*
    Two-way binding
        refers to the domino effect of changing the state going into the change of the display/UI in the frontend
*/

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}